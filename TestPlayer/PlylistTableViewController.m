//
//  PlylistTableViewController.m
//  Example
//
//  Created by Liliya Kiyamutdinova on 17/04/14.
//  Copyright (c) 2014 Gangverk. All rights reserved.
//

#import "PlylistTableViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AudioToolbox/AudioToolbox.h>

#import "GVMusicPlayerController.h"

@interface PlylistTableViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *audioArray;
//player
@property (strong, nonatomic) AVPlayer *player;

@property (nonatomic) AVPlayerItem *nowPlayingItem;
@property (nonatomic) MPMusicPlaybackState playbackState;
@property (nonatomic) MPMusicRepeatMode repeatMode; // note: MPMusicRepeatModeDefault is not supported
@property (nonatomic) MPMusicShuffleMode shuffleMode; // note: only MPMusicShuffleModeOff and MPMusicShuffleModeSongs are supported
@property (nonatomic) float volume; // 0.0 to 1.0
@property (nonatomic) NSUInteger indexOfNowPlayingItem; // NSNotFound if no queue
@property (nonatomic) BOOL isLoadingAsset;

@end

@implementation PlylistTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self formTableView];
    [self parseFolder];
}

- (void)formTableView
{
    [self.tableView registerClass: [UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    self.tableView.backgroundColor = [UIColor blackColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:(CGRect){CGPointZero, CGSizeMake(self.view.bounds.size.width, 10)}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Folder Parsing
- (void)setAudioArray:(NSArray *)audioArray{
    if(_audioArray != audioArray){
        _audioArray = audioArray;
        [self.tableView reloadData];
    }
}

- (void)parseFolder
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[NSMutableArray new] forKey:@"mp3"];
    [self parseForMp3:dic inPath:documentsPath forFolder:documentsPath];
//    NSLog(@"%@", dic);
    self.audioArray = dic[@"mp3"];
}

- (BOOL)isArtistName:(NSString*)file
{
    return [file hasPrefix:@"Artist"];
}

- (BOOL)isAlbumName:(NSString*)file
{
    return [file hasPrefix:@"Album"];
}

- (BOOL)isMp3File:(NSString*)file
{
    return [file hasSuffix:@".mp3"];
}

- (BOOL)isHiddenFile:(NSString*)file
{
    return [file hasPrefix:@"."];
}

- (void)parseForMp3:(NSMutableDictionary*)dic inPath:(NSString*)currentPath forFolder:(NSString*)folder
{
    BOOL comboBreak = false;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError* error;
    NSArray* files = [fileManager contentsOfDirectoryAtPath:currentPath error:&error];
    if (error)
    {
        //throw error or anything
    }
    for (NSString *file in files)
    {
        BOOL isDirectory = false;
        NSString *fullPath = [NSString stringWithFormat:@"%@/%@", currentPath, file];
        [fileManager fileExistsAtPath:fullPath isDirectory:&isDirectory];
        comboBreak = comboBreak || (!isDirectory &&
                                    ![self isMp3File:file] &&
                                    ![self isHiddenFile:file]);
        if (comboBreak)
            break;
    }
    for (NSString *file in files)
    {
        BOOL isDirectory = false;
        NSString *fullPath = [NSString stringWithFormat:@"%@/%@", currentPath, file];
        [fileManager fileExistsAtPath:fullPath isDirectory:&isDirectory];
        if (isDirectory)
        {
            if (!comboBreak)
            {
                [self parseForMp3:dic inPath:fullPath forFolder:folder];
            }
            else
            {
                [self parseForMp3:dic inPath:fullPath forFolder:fullPath];
            }
        }
        else if ([self isMp3File:file])
        {
            NSNumber *oldValue = [dic valueForKey:folder];
            oldValue = [NSNumber numberWithUnsignedInteger:[oldValue unsignedIntegerValue] + 1];
            [dic setValue:oldValue forKey:folder];
            
            NSMutableArray *mp3s = [[dic valueForKey:@"mp3"] mutableCopy];
            [mp3s addObject:@{@"fullPath":fullPath, @"title":file}];
            [dic setValue:mp3s forKey:@"mp3"];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.audioArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = self.audioArray[indexPath.row][@"title"];
    cell.textLabel.textColor = [UIColor redColor];
    return cell;
}

#pragma mark - Player Play
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *path = self.audioArray[indexPath.row][@"fullPath"];
    [self setupAVPlayerForURL:[NSURL fileURLWithPath:path]];

}

-(void) setupAVPlayerForURL: (NSURL*) url {
    AVAsset *asset = [AVURLAsset URLAssetWithURL:url options:nil];
    AVPlayerItem *anItem = [AVPlayerItem playerItemWithAsset:asset];
    
    self.player = [AVPlayer playerWithPlayerItem:anItem];
    [self.player addObserver:self forKeyPath:@"status" options:0 context:nil];
//    self.nowPlayingItem = anItem;
}

//- (void)setNowPlayingItem:(AVPlayerItem *)nowPlayingItem{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
//    
//    AVPlayerItem *previousTrack = _nowPlayingItem;
//    _nowPlayingItem = nowPlayingItem;
//    
//    // Used to prevent duplicate notifications
//    self.isLoadingAsset = YES;
//    
//    // Create a new player item
//    NSURL *assetUrl = [nowPlayingItem valueForProperty:MPMediaItemPropertyAssetURL];
//    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:assetUrl];
//    
//    // Either create a player or replace it
//    if (self.player) {
//        [self.player replaceCurrentItemWithPlayerItem:playerItem];
//    } else {
//        self.player = [AVPlayer playerWithPlayerItem:playerItem];
//    }
//    
//    // Subscribe to the AVPlayerItem's DidPlayToEndTime notification.
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAVPlayerItemDidPlayToEndTimeNotification) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
//    
//    // Inform delegates
////    for (id <GVMusicPlayerControllerDelegate> delegate in self.delegates) {
////        if ([delegate respondsToSelector:@selector(musicPlayer:trackDidChange:previousTrack:)]) {
////            [delegate musicPlayer:self trackDidChange:nowPlayingItem previousTrack:previousTrack];
////        }
////    }
//    
//    // Inform iOS now playing center
////    [self doUpdateNowPlayingCenter];
//    
//    self.isLoadingAsset = NO;
//}
#pragma mark - Emulate MPMusicPlayerController

- (void)skipToNextItem {
    if (self.indexOfNowPlayingItem+1 < [self.audioArray count]) {
        // Play next track
        self.indexOfNowPlayingItem++;
    } else {
        if (self.repeatMode == MPMusicRepeatModeAll) {
            // Wrap around back to the first track
            self.indexOfNowPlayingItem = 0;
        } else {
            if (self.playbackState == MPMusicPlaybackStatePlaying) {
                if (_nowPlayingItem != nil) {
//                    for (id <GVMusicPlayerControllerDelegate> delegate in self.delegates) {
//                        if ([delegate respondsToSelector:@selector(musicPlayer:endOfQueueReached:)]) {
//                            [delegate musicPlayer:self endOfQueueReached:_nowPlayingItem];
//                        }
//                    }
                }
            }
            NSLog(@"GVMusicPlayerController: end of queue reached");
            [self stop];
        }
    }
}

- (void)skipToBeginning {
    self.currentPlaybackTime = 0.0;
}

- (void)skipToPreviousItem {
    if (self.indexOfNowPlayingItem > 0) {
        self.indexOfNowPlayingItem--;
//    } else if (self.shouldReturnToBeginningWhenSkippingToPreviousItem) {
//        [self skipToBeginning];
    }
}

#pragma mark - MPMediaPlayback

- (void)play {
    [self.player play];
    self.playbackState = MPMusicPlaybackStatePlaying;
}

- (void)pause {
    [self.player pause];
    self.playbackState = MPMusicPlaybackStatePaused;
}

- (void)stop {
    [self.player pause];
    self.playbackState = MPMusicPlaybackStateStopped;
}

- (NSTimeInterval)currentPlaybackTime {
    return self.player.currentTime.value / self.player.currentTime.timescale;
}

- (void)setCurrentPlaybackTime:(NSTimeInterval)currentPlaybackTime {
    CMTime t = CMTimeMake(currentPlaybackTime, 1);
    [self.player seekToTime:t];
}

- (float)currentPlaybackRate {
    return self.player.rate;
}

- (void)setCurrentPlaybackRate:(float)currentPlaybackRate {
    self.player.rate = currentPlaybackRate;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (object == self.player && [keyPath isEqualToString:@"status"]) {
        if (self.player.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");
        } else if (self.player.status == AVPlayerStatusReadyToPlay) {
            NSLog(@"AVPlayer Ready to Play");
        } else if (self.player.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
        }
    }
}

-(IBAction) BtnPlay:(id)sender {
    [self.player play];
}

-(IBAction) BtnPause:(id)sender {
    [self.player pause];
}
@end
