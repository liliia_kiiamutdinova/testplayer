//
//  main.m
//  TestPlayer
//
//  Created by Liliya Kiyamutdinova on 17/04/14.
//  Copyright (c) 2014 High Technology Center Limited Company. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
